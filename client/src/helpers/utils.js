const calculateAge = (birthday) => {
  // birthday is a string in format YYYYMMDD
  const birthDate = new Date(birthday);
  const ageDifMs = Date.now() - birthDate.getTime();
  const ageDate = new Date(ageDifMs);

  return Math.abs(ageDate.getUTCFullYear() - 1970);
};

const randomNumber = (minimum, maximum) => {
  return Math.round(Math.random() * (maximum - minimum) + minimum);
};

const createCard = (birthDay) => {
  return {
    cardNumber: `${randomNumber(0000, 9999)} - ${randomNumber(
      0000,
      9999
    )} - ${randomNumber(0000, 9999)} - ${randomNumber(0000, 9999)}`,
    expirationDate: `${randomNumber(01, 12)}/${randomNumber(21, 35)}`,
    securityCode: `${randomNumber(000, 999)}`,
    type: calculateAge(birthDay) > 30 ? 'Gold' : 'Classic',
  };
};

const chooseGift = (date) => {
	let birthday = date;
	const month = parseInt(birthday.split('-')[1]);
	const day = parseInt(birthday.split('-')[2]);
	birthday = new Date(`2022-${month}-${day}`);

	let gift;
	if (birthday >= new Date('2021-12-21') && birthday < new Date('2022-03-20'))
		gift = 'Shirt'; // summer
	if (birthday >= new Date('2022-03-20') && birthday < new Date('2022-06-21'))
		gift = 'Sweater'; // fall
	if (birthday >= new Date('2022-06-21') && birthday < new Date('2022-09-21'))
		gift = 'Hoody'; // winter
	if (birthday >= new Date('2022-09-21') && birthday < new Date('2022-12-21'))
		gift = 'T-Shirt'; // spring

	return gift;
};

module.exports = { createCard, calculateAge, chooseGift };

