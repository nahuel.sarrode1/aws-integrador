const { eventCardSchema } = require('../schema/input/eventCardSchema');
const { cardServiceSchema } = require('../schema/output/cardServiceSchema');
const { createCard } = require('../../helpers/utils');
const { getClient, addCardToClient } = require('../service/cardService');

module.exports = async (eventPayload, eventMeta) => {
  const data = JSON.parse(eventPayload.Message);
  const payload = new eventCardSchema(data, eventMeta).get();  
  let client = await getClient(payload.dni);
  const card = createCard(client.date);
    
  await addCardToClient(new cardServiceSchema({ dni: client.dni, card }, eventMeta));
  
  return {
    statusCode: 200,
    body: 'Card created correctly'
  }
}