const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');

// Config Variables
const CANDIDATES = config.get('CANDIDATES');

const getClient = async (dni) => {
  try {
    const dbParams = { 
      TableName:  CANDIDATES,
      Key: { dni }
    }

    const { Item } = await dynamo.getItem(dbParams);
    
    return Item;
  } catch (error) {
    console.log(`Error finding element into db: ${error}`);
    throw error;
  }
}

const addCardToClient = async (client) => {
  try {
    const { commandPayload } = client.get();
    const card = commandPayload.card;
    const dni = commandPayload.dni;
    delete commandPayload.dni;

    const dbParams = {
      TableName: CANDIDATES,
      Key: { dni },
      UpdateExpression: 'SET #cr = :card',
      ExpressionAttributeNames: { '#cr': 'card' },
      ExpressionAttributeValues: { ':card': card },
      ReturnValues: 'ALL_NEW',
    };

    /* Object.keys(commandPayload).forEach(key => {
      dbParams.UpdateExpression += ` #${key}=:${key},`;
      dbParams.ExpressionAttributeNames[`#${key}`] = key;
      dbParams.ExpressionAttributeValues[`:${key}`] = commandPayload[key];
    });
    dbParams.UpdateExpression = dbParams.UpdateExpression.slice(0, -1); */
    
    const { Attributes } = await dynamo.updateItem(dbParams); 

    return Attributes;
  } catch (error) {
    console.log(`Error updating client in dynamo: ${error}`);
    throw error;
  }
}


module.exports = { getClient, addCardToClient }