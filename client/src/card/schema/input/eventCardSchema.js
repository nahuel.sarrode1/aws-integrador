const { InputValidation } = require('ebased/schema/inputValidation');

class eventCardSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'cardCreateSchema',
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true},      
      }
    });
  }
}

module.exports = { eventCardSchema };