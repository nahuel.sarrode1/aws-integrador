const { DownstreamCommand } = require('ebased/schema/downstreamCommand');

class cardServiceSchema extends DownstreamCommand {
  constructor(payload, meta) {
    super({
      meta: meta,
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true},
        card: {
          cardNumber: { type: String, required: true},
          securityCode: { type: String, required: true },
          expirationDate: { type: String, required: true },
        }
      }
    });
  }
};

module.exports = {cardServiceSchema};