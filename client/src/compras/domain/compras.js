const { getById, saveCompra } = require('../service/compraService');
const comprasInputSchema =  require('../schema/input/comprasInputSchema');

const calculateDiscount = (discountType, products) => {
  let percentage = discountType === 'classic' ? 0.08 : 0.12;

  return products.map((product) => {
    return { final: ((Number(product.price)) - (Number(product.price) * percentage)), ...product };
  })
};

const calculatePoints = (products) => {
  let total = products.reduce((acc, iterator) => {
    return acc += iterator.final;
  }, 0);

  return total = Math.round((total / 200));
} 

const addBuy = (client, total, products) => {
  let list = client.compras || { points: 0, operations: [] };
  list.points+= total;
  list.operations.push({ created_at: new Date().toString(), products });

  return list;
}

module.exports = async (commandPayload, commandMeta) => {
  const payload = new comprasInputSchema(commandPayload, commandMeta).get();

  if (payload.dni === '' || payload.dni === ' ' || !payload.products.length) {
    return { statusCode: 400, body: 'You should specify an id client and products' }
  }

  const client = await getById(payload.dni);
  if (!client) return { statusCode: 400, body: 'The user doesnt exist' };

  const products = calculateDiscount(client.card.type, payload.products);
  const total = calculatePoints(products);
  const list = addBuy(client, total, products);
   
  await saveCompra({dni: client.dni, list});

  return {
    statusCode: 200,
    body: 'Your buy was accomplish successfully'
  }
} 