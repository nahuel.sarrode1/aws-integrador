const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');
const CANDIDATES = config.get('CANDIDATES');

const getById = async (dni) => {
  try {
    const params = { TableName: CANDIDATES, Key: { dni }}
    const { Item } = await dynamo.getItem(params);

    return Item;
  } catch (error) {
    console.log(`Error getting user by dni ${error}`);
    throw error;
  }
}

const saveCompra = async ({ dni, list }) => {
  try {
    const params = { 
      TableName: CANDIDATES,
      Key: { dni },
      UpdateExpression: 'SET #cs = :compras',
      ExpressionAttributeNames: { '#cs': 'compras' },
      ExpressionAttributeValues: { ':compras': list },
      ReturnValues: 'ALL_NEW',
    }

    await dynamo.updateItem(params);
  } catch (error) {
    console.log(`Error putting compras into user ${error}`);
    throw error;
  }
}

module.exports = { getById, saveCompra }