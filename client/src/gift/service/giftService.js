const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config');
const CANDIDATES = config.get('CANDIDATES');

const getClient = async (dni) => {
  try {
    const params = { TableName: CANDIDATES, Key: { dni }};
    const { Item } = await dynamo.getItem(params);
    
    return Item;
  } catch (error) {
    console.log(`Error getting client by id into gift service: ${error}`);
    throw error;
  }
};

const updateClient = async (data) => {
  try {
    const { commandPayload } = data.get();
    const { dni, gift } = commandPayload;
    
    const dbParams = {
      TableName: CANDIDATES,
      Key: { dni },
      UpdateExpression: 'SET #gf = :gift',
      ExpressionAttributeNames: { '#gf': 'gift' },
      ExpressionAttributeValues: { ':gift': gift },
      ReturnValues: 'ALL_NEW',
    };

   /*  Object.keys(commandPayload).forEach(key => {
      dbParams.UpdateExpression += ` #${key}=:${key},`;
      dbParams.ExpressionAttributeNames[`#${key}`] = key;
      dbParams.ExpressionAttributeValues[`:${key}`] = commandPayload[key];
    }); */

    // get a clean object without trash to update a regiter.
    //dbParams.UpdateExpression = dbParams.UpdateExpression.slice(0, -1);
    
    await dynamo.updateItem(dbParams);     
  } catch (error) {
    console.log(`Error updating client by id into gift service: ${error}`);
    throw error; 
  }
};

module.exports = { getClient, updateClient};