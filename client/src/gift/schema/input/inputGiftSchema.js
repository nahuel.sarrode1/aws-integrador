const { InputValidation } = require('ebased/schema/inputValidation');

class inputGiftSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      type: 'createGiftLambda',
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true},      
      }
    })
  }
}

module.exports = inputGiftSchema;