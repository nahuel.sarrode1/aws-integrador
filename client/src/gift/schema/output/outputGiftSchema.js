const { DownstreamCommand } = require('ebased/schema/downstreamCommand');

class outputGiftSchema extends DownstreamCommand {
  constructor(payload, meta) {
    super({
      meta: meta,
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true},
        gift: {
          name: { type: String, required: true},
          bday: { type: String, required: true}
        },
      }
    });
  }
}

module.exports = outputGiftSchema; 