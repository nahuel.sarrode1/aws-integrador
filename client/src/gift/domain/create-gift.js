const inputGiftSchema = require('../schema/input/inputGiftSchema');
const outputGiftSchema = require('../schema/output/outputGiftSchema');
const { getClient, updateClient } = require('../service/giftService');
const { chooseGift } = require('../../helpers/utils');

module.exports = async (eventPayload, eventMeta) => {
  try {
    const data = JSON.parse(eventPayload.Message);
    const payload = new inputGiftSchema(data, eventMeta).get();

    const client = await getClient(payload.dni);
    const giftName = chooseGift(client.date);
    const gift = { name: giftName, bday: client.date }

    await updateClient(new outputGiftSchema({ dni: client.dni, gift: gift }, eventMeta));

    return {
      statusCode: 200,
      body: "giftCreated"
    }
  } catch (error) {
    console.log(`Error into create-gift domain: ${error}`);
    throw error;
  }
};