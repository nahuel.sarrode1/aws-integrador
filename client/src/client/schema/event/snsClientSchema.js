const { DownstreamEvent } = require('ebased/schema/downstreamEvent');

class snsClientSchema extends DownstreamEvent {
  constructor(payload, meta) {
    super({
      meta: meta,
      type: 'clientCreate',
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: false,
        dni: { type: String, required: true},      }
    });
  }
}

module.exports = snsClientSchema;