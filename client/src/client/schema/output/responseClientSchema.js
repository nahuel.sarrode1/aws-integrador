const { DownstreamCommand } = require('ebased/schema/downstreamCommand');

class responseClientSchema extends DownstreamCommand {
  constructor(payload, meta) {
    super({
      meta: meta,
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: true,
        dni: { type: String, required: true },
        name: { type: String, required: true },
        lastname: { type: String, required: true },
        email: { type: String, required: true },
        date: { type: String, required: true },
        gift: { type: String, required: true },
        cardNumber: { type: String, required:true },
        expirationDate: { type: String, required:true },
        securityCode: { type: String, required:true },
        type: { type: String, required:true },
      }
    });
  }
}

module.exports = responseClientSchema;