const { InputValidation } = require('ebased/schema/inputValidation');

class requestClientByIdSchema extends InputValidation {
  constructor(payload, meta) {
    super({
      meta: meta,
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: true,
        dni: { type: String, required: true },
      }
    });
  }
}

module.exports = requestClientByIdSchema;