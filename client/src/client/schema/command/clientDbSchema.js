const { DownstreamCommand } = require('ebased/schema/downstreamCommand');

class clientDbSchema extends DownstreamCommand {
  constructor(payload, meta) {
    super({
      source: meta.status,
      payload: payload,
      specversion: "v1.0.0",
      schema: {
        strict: true,
        dni: { type: String, required: true},
        name: { type: String, required: true },
        lastname: { type: String, required: true },
        email: { type: String, required: true },
        date: { type: String, required: true },
        status: { type: String, required: true }
      }
    });
  }
};

module.exports = clientDbSchema;