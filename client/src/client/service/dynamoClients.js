const dynamo = require('ebased/service/storage/dynamo');
const config = require('ebased/util/config'); 

const CANDIDATES = config.get('CANDIDATES');

const createClient = async (candidate) => { 
  const { commandPayload } = candidate.get();

  return await dynamo.putItem({ 
    TableName: CANDIDATES, 
    Item: {
      "dni": commandPayload.dni,
      "name": commandPayload.name ,
      "lastname": commandPayload.lastname,
      "email": commandPayload.email ,
      "date": commandPayload.date,
      "status": commandPayload.status,
    }
  });
};

const changeClientStatus = async (dni, status) => {
  try {
    const params = {
      TableName: CANDIDATES,
      Key: { dni: dni }, 
      UpdateExpression: 'set #st = :status',
      ExpressionAttributeNames: { '#st': 'status' },
      ExpressionAttributeValues: { ':status': status }
    }

    const { Item } = await dynamo.updateItem(params);

    return Item;
  } catch (error) {
    console.log(`Error updating client into service: ${error}`);
    throw error;
  }
}

const getClientById = async ({ dni }) => {
  try {
    const params = {
      TableName: CANDIDATES,
      Key: { dni }
    };

    const { Item } = await dynamo.getItem(params);

    return Item;
  } catch (error) {
    console.log(`Error getting client by id into getClient lambda: ${error}`);
    throw error;
  }
}

const getAll = async () => {
  try {
    const { Items } = await dynamo.queryTable({
      TableName: CANDIDATES,
      IndexName: 'userStatus',
      KeyConditionExpression: "#uss = :active",
      ExpressionAttributeNames: { "#uss": "status" },
      ExpressionAttributeValues: { ":active": "active" }
    });
    
    return Items;
  } catch (error) {
    console.log(`Error getting clients into getAll service: ${error}`);
    throw error;
  }
}

module.exports = { createClient, getClientById, getAll, changeClientStatus };