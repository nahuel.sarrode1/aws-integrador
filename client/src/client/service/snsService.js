const sns = require('ebased/service/downstream/sns'); 
const config  = require('ebased/util/config'); 
const SNSTOPIC = config.get('SNSTOPIC');

const pushNotification = async (clientCreatedEvent) => {
  const { eventPayload, eventMeta } = clientCreatedEvent.get();
  const snsParams = {
    Message: eventPayload,
    TopicArn: SNSTOPIC
  }

  await sns.publish(snsParams, eventMeta);
}

module.exports = { pushNotification };


