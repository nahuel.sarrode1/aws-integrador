const requestClientByIdSchema = require('../schema/input/requestClientByIdSchema');
const { getClientById, changeClientStatus } = require('../service/dynamoClients');

module.exports = async (commandPayload, commandMeta) => {
  try {
    let response;
    if (!commandPayload.dni || commandPayload.dni === ' ' || commandPayload.dni === '') {
      return response = { statusCode: 400, body: 'You must send an dni' }
    }

    const payload = new requestClientByIdSchema(commandPayload, commandMeta).get();
    const client = await getClientById(payload);
  
    if (!client) return response = { body: 'The provided id doesnt exist' }; 

    const updated = await changeClientStatus(client.dni, 'inactive');

    return response = { body: 'The client has been desactivated!' }
  } catch (error) {
    console.log(`Error into delete client domain: ${error}`);
    throw error;
  }
};