const { createClient, getClientById } = require('../service/dynamoClients'); 
const { pushNotification } = require('../service/snsService');
const { calculateAge } = require('../../helpers/utils');

// Schemas
const requestClientSchema = require('../schema/input/requestClientSchema');
const clientDbSchema = require('../schema/command/clientDbSchema');
const snsClientSchema = require('../schema/event/snsClientSchema');

module.exports = async (commandPayload, commandMeta) => {
  let response; 
  const candidate = new requestClientSchema(commandPayload, commandMeta).get();
  
  const exist = await getClientById(candidate);
  if (exist) return response = { statusCode: 400, body: 'User with this dni already registered try to reset your password' }

  if (calculateAge(candidate.date) > 65) {
    return response = {
      statusCode: 400,
      body: JSON.stringify('Users should have less than 65 years old'),
    };
  }

  commandPayload.status = 'active';
  const { Item } = await createClient(new clientDbSchema(commandPayload, commandMeta));
  await pushNotification(new snsClientSchema(Item, commandMeta));
  
  return response = {
    body: {
      message: "The Client has been created"
    }
  }
};