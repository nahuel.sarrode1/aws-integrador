const { getAll } = require('../service/dynamoClients');

module.exports = async (commandPayload, commandMeta) => {
  try {
    const items = await getAll(); 

    return {
      statusCode: 200,
      body: items
    }
  } catch (error) {
    console.log(`Error into get-all domain: ${error}`);
    throw error;
  }
}