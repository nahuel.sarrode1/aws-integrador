const { getClientById } = require('../service/dynamoClients');
const requestClientByIdSchema = require('../schema/input/requestClientByIdSchema');
const responseClientSchema = require('../schema/output/responseClientSchema');

module.exports = async (commandPayload, commandMeta) => {
  try {
    let response;
    if (!commandPayload.dni || commandPayload.dni === ' ' || commandPayload.dni === '') {
      return response = { statusCode: 400, body: 'You must send an dni' }
    }

    const payload = new requestClientByIdSchema(commandPayload, commandMeta).get();
    const client = await getClientById(payload);
    
    if (!client) return response = { statusCode: 400, body: 'The user doesnt exist' }

    const data = new responseClientSchema(client, commandMeta).get();
    (!data.commandPayload) ? response = { statusCode: 400, body: 'There isnt any client with this id' } : 
    response = { statusCode: 200, body: data.commandPayload }

    return response;
  } catch (error) {
    console.log(`Error into getClient domain: ${error}`);
    throw error;
  }
};