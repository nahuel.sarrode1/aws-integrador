const { commandMapper } = require('ebased/handler');
const inputMode = require('ebased/handler/input/commandApi');
const outputMode = require('ebased/handler/output/commandApi');
const getAllDomain = require('../domain/get-all');

module.exports.handler = async (command, context) => {
  try {
    return commandMapper(
      {command, context},
      inputMode,
      getAllDomain,
      outputMode
    );
  } catch (error) {
    console.log(`Error into get-all handler: ${error}`);
    throw error;
  }
}